# Introduction to Development
This repository contains the complete workshop of DevOps Introduction to Development module.
It is advised to follow the content as indicated by the index number in the folder name.
For example content of folder staring with `009_...` should be processed before content 
folder starting with `011_...`.

## Theory and Demos
These folders contain portions of the theoratical matherial and demonstrations.

## Homework
Homeworks are meant to be done after the workshop. These are similar exercises to the completed
challenges, but are mixed up a bit in order to give you a chance to try out the leaned technologies/techniques.

## Recordings
You will have access to the recording of the workshop activities. These recordings will be shared with you later on this page.

## Knowledge Check Quiz
You will receive a link during the week of the workshops to participate in a knowdeldge check quiz.
Please participage to get feedback about how well you learned the material of this week.

## Prerequisites
You should have 
* GitLab account
* Heroku account

## Tools used throughout the workshop demos, and homework assingments
* Katacoda: katacoda.com
* Gitpod (gitlab advanced webide)
